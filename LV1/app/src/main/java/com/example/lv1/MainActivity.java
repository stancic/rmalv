package com.example.lv1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Toast toast;
    public String aboutBalic , aboutRonaldo, aboutLebron;
    private ImageView ivBalicImage, ivRonaldoImage, ivLebronImage;
    private CardView cvBalicCard, cvRonaldoCard, cvLebronCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }
       

    private void initializeUI () {
        ivBalicImage = (ImageView) findViewById(R.id.balicImage);
        ivRonaldoImage = (ImageView) findViewById(R.id.ronaldoImage);
        ivLebronImage = (ImageView) findViewById(R.id.lebronImage);
        cvBalicCard = (CardView) findViewById(R.id.balicCard);
        cvRonaldoCard = (CardView) findViewById(R.id.ronaldoCard);
        cvLebronCard = (CardView) findViewById(R.id.lebronCard);
        aboutBalic = (String) getApplicationContext().getString(R.string.aboutBalic);
        aboutRonaldo = (String) getApplicationContext().getString(R.string.aboutRonaldo);
        aboutLebron = (String) getApplicationContext().getString(R.string.aboutLebron);

        ivBalicImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelToast();
                showToast(getApplicationContext().getString(R.string.balicToast));
            }
        });

        ivRonaldoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelToast();
                showToast(getApplicationContext().getString(R.string.ronaldoToast));
            }
        });

        ivLebronImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelToast();
                showToast(getApplicationContext().getString(R.string.lebronToast));
            }
        });

        cvBalicCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAboutStarsActivity(aboutBalic, ivBalicImage);
            }
        });

        cvRonaldoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAboutStarsActivity(aboutRonaldo, ivRonaldoImage);
            }
        });

        cvLebronCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAboutStarsActivity(aboutLebron, ivLebronImage);
            }
        });
    }

    void cancelToast () {
        if(toast != null){
            toast.cancel();
        }
    }

    void showToast(String toastMessage) {
        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void openAboutStarsActivity(String aboutText, ImageView image) {
        Intent intent = new Intent(getBaseContext(), AboutStarsActivity.class);
        image.buildDrawingCache();
        Bitmap bitmap = image.getDrawingCache();
        intent.putExtra("ABOUT_TEXT", aboutText);
        intent.putExtra("BITMAP_IMAGE", bitmap);
        startActivity(intent);
    }

}