package com.example.lv1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutStarsActivity extends AppCompatActivity {

    private TextView tvAbout;
    private ImageView ivAbout;
    Intent intent;
    Bitmap bitmap;
    String aboutText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_stars);
        initializeUI();
    }

    private void initializeUI() {
        tvAbout = (TextView) findViewById(R.id.aboutText);
        ivAbout = (ImageView) findViewById(R.id.aboutImage);
        intent = (Intent) getIntent();
        tvAbout.setText(intent.getStringExtra("ABOUT_TEXT"));
        bitmap = (Bitmap) intent.getParcelableExtra("BITMAP_IMAGE");
        ivAbout.setImageBitmap(bitmap);
    }
}